#!/usr/bin/env python
# -*- coding:utf-8 -*-
import multiprocessing
import os

__all__ = ['STATIC_PATH', 'MODEL_PATH', 'DEFAULT_CONFIG', 'DEFAULT_DB', 'EMBEDDING_DIM', 'MAX_SEQUENCE_LENGTH',
           'CPU_COUNT']

_BASE_PATH = os.path.dirname(os.path.abspath(__file__))

STATIC_PATH = os.path.join(_BASE_PATH, 'static')

MODEL_PATH = os.path.join(_BASE_PATH, 'static', 'model')

EMBEDDING_DIM = 100
MAX_SEQUENCE_LENGTH = 200
CPU_COUNT = multiprocessing.cpu_count()

DEFAULT_CONFIG = {
    'fit': dict(epoch=20, batch_size=32, verbose=1, shuffle=True, validation_split=0.2,
                save=dict(save=False, model='model', word2vec='word2vec', tokenize='tokenizer')),
    'word2vec': dict(exposures=10, window_size=7, iterations=1, max_len=200),
    'db': dict(name='default', host='127.0.0.1', port=3306, user='root', password=None, database='crawler',
               charset='utf8')
}

DEFAULT_DB = {'last_id': None}

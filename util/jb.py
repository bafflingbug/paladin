#!/usr/bin/env python
# -*- coding:utf-8 -*-
from jieba import posseg

__all__ = ['fc']

_flag_list = ['t', 'q', 'p', 'u', 'e', 'y', 'o', 'w', 'm']  # 时间词，量词，介词，助词，叹词，语气词，拟声词，标点符号，数词


def fc(all_the_text):
    """分词函数，同时排除掉时间词，量词，介词，助词，叹词，语气词，拟声词，标点符号，数词"""
    re = ""
    words = posseg.cut(all_the_text)
    count = 0
    for w in words:
        flag = w.flag
        tmp = w.word
        if not (not (len(tmp) > 1) or not (len(flag) > 0) or not (flag[0] not in _flag_list)) and u'/u4e00' <= tmp[
            0] <= u'\u9fa5':
            re = re + " " + w.word
            count = count + 1
    re = re.replace("\r\n", "\n").replace("\n\n", "\n").replace("\n", " ").replace("\r", " ").strip()
    re = re + "\n"

    return re


if __name__ == '__main__':
    print(fc('酒店的环境非常好，价格也便宜，值得推荐'))
